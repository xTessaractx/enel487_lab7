#include "stdint.h"
#include "registers.h"

//Registers are defined in register.h and register.c
extern volatile uint32_t * regRCC_APB2ENR;	 
extern volatile uint32_t * regGPIOB_ODR;
extern volatile uint32_t * regGPIOB_CRH;
extern volatile uint32_t * regGPIOB_BSRR;
extern volatile uint32_t * regGPIOB_BRR;
  
//Serial
extern volatile uint32_t * regGPIOA_CRL;
extern volatile uint32_t * regGPIOA_ODR;
extern volatile uint32_t * regGPIOA_CRH;
extern volatile uint32_t * regGPIOA_BSRR;
extern volatile uint32_t * regGPIOA_BRR; 

//USART
extern volatile uint32_t * regRCC_APB1ENR;
extern volatile uint32_t * regUSART_CR1;
extern volatile uint32_t * regUSART_CR2;
extern volatile uint32_t * regUSART_BRR;
extern volatile uint32_t * regUSART_DR;
extern volatile uint32_t * regUSART_SR;

//TIM2
extern volatile uint32_t * regTIM2_CR1;
extern volatile uint32_t * regTIM2_CR2;
extern volatile uint32_t * regTIM2_ERG;
extern volatile uint32_t * regTIM2_CNT;
extern volatile uint32_t * regTIM2_CCR1;
extern volatile uint32_t * regTIM2_PSC;
extern volatile uint32_t * regTIM2_ARR;
extern volatile uint32_t * regTIM2_CCMR1;
extern volatile uint32_t * regTIM2_CCER;
extern volatile uint32_t * regTIM2_DIER;

//TIM4
extern volatile uint32_t * 	regTIM4_CR1;
extern volatile uint32_t * 	regTIM4_CR2;             
extern volatile uint32_t * 	regTIM4_SMCR;   
extern volatile uint32_t * 	regTIM4_CCMR1;     
extern volatile uint32_t * 	regTIM4_CCMR2;   
extern volatile uint32_t * 	regTIM4_CCER;    
extern volatile uint32_t * 	regTIM4_PSC;      
extern volatile uint32_t * 	regTIM4_ARR;      
extern volatile uint32_t * 	regTIM4_CCR1;        
extern volatile uint32_t * 	regTIM4_CCR2;   
extern volatile uint32_t * 	regTIM4_CCR3;  
extern volatile uint32_t * 	regTIM4_CCR4; 
extern volatile uint32_t * 	regTIM4_EGR;
extern volatile uint32_t * 	regGPIOB_CRL;
extern volatile uint32_t *  regTIM4_SR;
extern volatile uint32_t *  regTIM4_DMAR;

//ADC1
extern volatile uint32_t * 	regADC1_SR;
extern volatile uint32_t * 	regADC1_CR1;
extern volatile uint32_t * 	regADC1_CR2;
extern volatile uint32_t * 	regADC1_DR;
extern volatile uint32_t * 	regADC1_SQR3;
extern volatile uint32_t * 	regGPIOC_CRL;
extern volatile uint32_t *	regADC1_SMPR1;
extern volatile uint32_t *	regNVIC_ISER0;

static const int TO_UPPER_LOWER_BOUNDARY = 97;
static const int TO_UPPER_UPPER_BOUNDARY = 122;
static volatile uint32_t RESET_LIGHTS = 0x01000000;

void print_timer_all(uint32_t, char[], char [30]);
void determine_user_input (char data_array[50]);
void print_string(char [100]);
char TO_UPPER (char);
int Good_To_Go(int);
void get_status(int);
void on(int);
void off(int);

void print_time(void);
void print_date(void);

void restart(char data_array[50]);
void print_help(void);


//Labs 4 and 5
void stm32_TimerSetup (void);
void print_string_reverse(char output[], int length);
void ADC1_2_IRQHandler(void);
void a2d_init(void);
void int_to_char(uint16_t);
void intit_global(void);
int get_global_adc_output(void);

//Lab 6
void pwm_adc_close (void);
void pwm_adc_open (void);
void check_height (int);
void pwm_check_height (void);
int convert_cm_to_height(float);
