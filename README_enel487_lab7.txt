Authors: Tessa Herzberger and Kiley LeClair
Class: ENEL 487
Project: Lab 7
Due 23 November 2017

Version History:
	- The program was committed at the following times:
		- November 9th: First Commit (Files from Lab 6 added, initial errors repaired)
		- November 10th: Second Commit (All initialization functions working except for USART)
		- November 22: Nov 22 Commit (Fixed USART. Able to send to and receive from serial console)
		- November 22: BE THE BALL (ball controller working)
		- November 23: 3AM Commit (CLI Task added)

The Program:
	- This program takes the code from Lab 6 and modifies it work using a Real Time Operating System (in this case, FreeRTOS).
	- It consists of two queues:
		- xQueue (used to send and recieve the value of the ADC tasks)
		- xQueue PWM (used to modify the servo)
	- The program has the following tasks:
		- cli_task_tx
			- Runs the CLI code. Sends and receives nothing.
		- pwm_task_rx
			- Determines whether the ball's position needs to be increased or decreased.
			- Sends nothing.
			- Recieves the value of the difference between the desired ADC output and the actual ADC output from pwm_task_tx.
		- pwm_task_tx
			- Recieves the actual ADC output from the adc_task_rx
			- Sends the value of the difference between the desired ADC output and the actual ADC output to wm_task_rx.
		- adc_task_rx
			- Recieves the value of the ADC output from the adc_task_tx task
			- Sends the value of the ADC output to the pwm_task_tx task
		- adc_task_tx
			- Obtains the value of the ADC by triggering the ADC interrupt.
			- Recieves nothing.
			- Sends the value of the ADC output to the adc_task_rx task

Compiling:
	- To compile this program, use Keil Tools.