/**
Filename: pwm.c
Author: Tessa Herzberger and Kiley LeClair
Class: enel487
Project: lab6
*/

#include "main.h"

/**
		The stm32_TimerSetup functoin initializes the registers
		necissary for TIM4 to be used for Pulse Width Modulation (PWM).
		It takes no parameters and returns nothing.
*/
void stm32_TimerSetup (void)
{
	const int MAX_DC = 12;
	const int MIN_DC = 3;
	
	*regRCC_APB2ENR |= 0x8; //Turning on the IOPB
  * regGPIOB_CRL = 0xB0000000;
	*regRCC_APB1ENR |= 0x4;	//Enable the clock for TIM4
	*regTIM4_CR1  |= 0x00000001; //Turn on the ARPE and CEN bits 
	*regTIM4_EGR |= 0x1; //Turn on the UG bit	
	*regTIM4_CCMR1 |= 0x6800;      //Turn on the OCIM 2 and 1 bits amd OC1PE bit for channel 2
	*regTIM4_CCER  |= 0x00000010; //Turn on the Capture/Compare Enable bit for channel 2
	
	//counter frequency = 72MHz/(PSC + 1)
	//want counter frequency to be 5kHz
	//want period to be 20ms,
	//Therefore, use PSC = 1430.
	//Offset by 10000, which is why ARR = 10000
	*regTIM4_PSC = 143;	
	*regTIM4_ARR = 10000; //Set the ARR = 10000, because want higher resolution
	
	*regTIM4_CCR1 = 0;
	*regTIM4_CCR2 = 0;
	*regTIM4_CCR3 = 0;
	*regTIM4_CCR4 = 0;
	
	*regTIM4_CCR2 = (((MAX_DC - MIN_DC) * (60/100)) + MIN_DC) * 100;
	
	*regTIM4_CR1  |= 0x81; //Turn on the ARPE and CEN bits
	*regTIM4_EGR |= 0x1; //Turn on the UG bit
}
