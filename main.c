/*
    FreeRTOS V7.2.0 - Copyright (C) 2012 Real Time Engineers Ltd.


    ***************************************************************************
     *                                                                       *
     *    FreeRTOS tutorial books are available in pdf and paperback.        *
     *    Complete, revised, and edited pdf reference manuals are also       *
     *    available.                                                         *
     *                                                                       *
     *    Purchasing FreeRTOS documentation will not only help you, by       *
     *    ensuring you get running as quickly as possible and with an        *
     *    in-depth knowledge of how to use FreeRTOS, it will also help       *
     *    the FreeRTOS project to continue with its mission of providing     *
     *    professional grade, cross platform, de facto standard solutions    *
     *    for microcontrollers - completely free of charge!                  *
     *                                                                       *
     *    >>> See http://www.FreeRTOS.org/Documentation for details. <<<     *
     *                                                                       *
     *    Thank you for using FreeRTOS, and thank you for your support!      *
     *                                                                       *
    ***************************************************************************


    This file is part of the FreeRTOS distribution.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation AND MODIFIED BY the FreeRTOS exception.
    >>>NOTE<<< The modification to the GPL is included to allow you to
    distribute a combined work that includes FreeRTOS without being obliged to
    provide the source code for proprietary components outside of the FreeRTOS
    kernel.  FreeRTOS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details. You should have received a copy of the GNU General Public
    License and the FreeRTOS license exception along with FreeRTOS; if not it
    can be viewed here: http://www.freertos.org/a00114.html and also obtained
    by writing to Richard Barry, contact details for whom are available on the
    FreeRTOS WEB site.

    1 tab == 4 spaces!
    
    ***************************************************************************
     *                                                                       *
     *    Having a problem?  Start by reading the FAQ "My application does   *
     *    not run, what could be wrong?                                      *
     *                                                                       *
     *    http://www.FreeRTOS.org/FAQHelp.html                               *
     *                                                                       *
    ***************************************************************************

    
    http://www.FreeRTOS.org - Documentation, training, latest information, 
    license and contact details.
    
    http://www.FreeRTOS.org/plus - A selection of FreeRTOS ecosystem products,
    including FreeRTOS+Trace - an indispensable productivity tool.

    Real Time Engineers ltd license FreeRTOS to High Integrity Systems, who sell 
    the code with commercial support, indemnification, and middleware, under 
    the OpenRTOS brand: http://www.OpenRTOS.com.  High Integrity Systems also
    provide a safety engineered and independently SIL3 certified version under 
    the SafeRTOS brand: http://www.SafeRTOS.com.
*/

/*
 * This is a very simple demo that demonstrates task and queue usages only in
 * a simple and minimal FreeRTOS configuration.  Details of other FreeRTOS 
 * features (API functions, tracing features, diagnostic hook functions, memory
 * management, etc.) can be found on the FreeRTOS web site 
 * (http://www.FreeRTOS.org) and in the FreeRTOS book.
 *
 * Details of this demo (what it does, how it should behave, etc.) can be found
 * in the accompanying PDF application note.
 *
*/

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"

/* Standard include. */
#include <stdio.h>

/* Priorities at which the tasks are created. */
#define cli_tx_TASK_PRIORITY                ( tskIDLE_PRIORITY + 4 )
#define cli_rx_TASK_PRIORITY                ( tskIDLE_PRIORITY + 3 )
#define adcTASK_PRIORITY        	          ( tskIDLE_PRIORITY + 5 )
#define pwmTASK_PRIORITY                    ( tskIDLE_PRIORITY + 6 )
#define controlTASK_PRIORITY                ( tskIDLE_PRIORITY + 7 )

/* The rate at which data is sent to the queue, specified in milliseconds. */
#define mainQUEUE_SEND_FREQUENCY_MS         ( 10 / portTICK_RATE_MS )

/* The number of items the queue can hold.  This is 1 as the receive task
will remove items as they are added, meaning the send task should always find
the queue empty. */
#define mainQUEUE_LENGTH                    ( 5 )

/* The ITM port is used to direct the printf() output to the serial window in 
the Keil simulator IDE. */
#define mainITM_Port8(n)    (*((volatile unsigned char *)(0xE0000000+4*n)))
#define mainITM_Port32(n)   (*((volatile unsigned long *)(0xE0000000+4*n)))
#define mainDEMCR           (*((volatile unsigned long *)(0xE000EDFC)))
#define mainTRCENA          0x01000000

/*-----------------------------------------------------------*/

/*
 * The tasks as described in the accompanying PDF application note.
 */
static void cli_task_tx ( void *pvParameters );
static void pwm_task_tx ( void *pvParameters );
static void pwm_task_rx ( void *pvParameters );
static void adc_task_tx ( void *pvParameters );
static void adc_task_rx ( void *pvParameters );

/*
 * Redirects the printf() output to the serial window in the Keil simulator
 * IDE.
 */
int fputc( int iChar, FILE *pxNotUsed );

/*-----------------------------------------------------------*/

/* The queue used by both tasks. */
static xQueueHandle xQueue = NULL;
static xQueueHandle xQueue_pwm = NULL;
static xQueueHandle xQueue_pwm2 = NULL;

/* One array position is used for each task created by this demo.  The 
variables in this array are set and cleared by the trace macros within
FreeRTOS, and displayed on the logic analyzer window within the Keil IDE -
the result of which being that the logic analyzer shows which task is
running when. */
unsigned long ulTaskNumber[ configEXPECTED_NO_RUNNING_TASKS ];

/*-----------------------------------------------------------*/
#include "serial_driver_interface.h"
#include "timer.h"
#include <cmath>

int main(void)
{
	
    setupRegs();
		serial_open();
		timer_init();
		stm32_TimerSetup();
		a2d_init();
		float init_duty_cycle = 30;
		const int MAX_DC = 12;
		const int MIN_DC = 3;
	
    //Need to setup the the LED's on the board
    * regRCC_APB2ENR |= 0x08; // Enable Port B clock
    * regGPIOB_ODR  &= ~0x0000FF00;          /* switch off LEDs                    */
    * regGPIOB_CRH  = 0x33333333; //50MHz  General Purpose output push-pull
		
	
    /* Create the queue. */
    xQueue = xQueueCreate( mainQUEUE_LENGTH, sizeof( int ) );
		xQueue_pwm = xQueueCreate( mainQUEUE_LENGTH, sizeof( int ) );
		xQueue_pwm2 = xQueueCreate( mainQUEUE_LENGTH, sizeof( int ) );
	
	
		init_duty_cycle  = ((MAX_DC - MIN_DC) * (init_duty_cycle/100)) + MIN_DC;
		change_duty_pwm(init_duty_cycle);
				
if( xQueue != NULL )
    {

				xTaskCreate( cli_task_tx, (signed char *) "CLI_TASK_TX",
                 configMINIMAL_STACK_SIZE, NULL,
                 cli_tx_TASK_PRIORITY, NULL );
        xTaskCreate( adc_task_tx, (signed char *) "ADC_TASK_TX",
                 configMINIMAL_STACK_SIZE, NULL,
                 adcTASK_PRIORITY, NULL );
        xTaskCreate( adc_task_rx, (signed char *) "ADC_TASK_RX",
                 configMINIMAL_STACK_SIZE, NULL,
                 adcTASK_PRIORITY, NULL );
				xTaskCreate( pwm_task_tx, (signed char *) "PWM_TASK_TX",
								 configMINIMAL_STACK_SIZE, NULL,
                 controlTASK_PRIORITY, NULL );
        xTaskCreate( pwm_task_rx, (signed char *) "PWM_TASK_RX",
                 configMINIMAL_STACK_SIZE, NULL,
                 pwmTASK_PRIORITY, NULL );
				
        vTaskStartScheduler();
    }

    /* If all is well we will never reach here as the scheduler will now be
    running.  If we do reach here then it is likely that there was insufficient
    heap available for the idle task to be created. */
    for( ;; )
        ;
}
/*-----------------------------------------------------------*/

/**
		The adc_task_rx task obtains the value of
		the ADC by triggering the ADC interrupt.
		It takes a void pointer.
		It returns nothing.
*/
static void adc_task_tx( void *pvParameters )
{
		int adc_output = 0;
    
    while (1)
		{	
			//Set the SWSTART bit to trigger the ADC conversion;
			*regADC1_CR2 |= 0x00400000;
			
			adc_output = get_global_adc_output();
			
			if (!xQueueSend(xQueue, &adc_output, 100))
			{
				print_string("Failed to send to adc queue.");
				print_string("\n\r");
			}
    }
}

/**
		The adc_task_rx task recieves the value of
		the ADC output from the adc_task_tx task
		It then sends the value of the ADC output
		to the pwm_task_tx task.
		It takes a void pointer.
		It returns nothing.
*/
static void adc_task_rx( void *pvParameters )
{
		int adc_output;
    
    while (1)
		{
			if (xQueueReceive(xQueue, &adc_output, 1000))
			{			
				if (!xQueueSend(xQueue_pwm, &adc_output, 1000))
				{
					print_string("Failed to send to pwm queue.");
					print_string("\n\r");
				}
			}
			else
			{
				print_string("Failed to recieve from adc queue.");
				print_string("\n\r");
			}	
    }
}

/**
		The pwm_task_tx task recieves the actual
		ADC output from the adc_task_rx.
		It then sends the value of the difference
		between the desired ADC output and the actual
		ADC output to pwm_task_rx.
		It takes a void pointer.
		It returns nothing.
*/
static void pwm_task_tx( void *pvParameters )
{
	int adc_output = 0;
	int difference = 0;
	int wanted_adc_output = 0;  
			
	while (1)
	{	
		if (xQueueReceive(xQueue_pwm, &adc_output, 1000))
		{
			wanted_adc_output = convert_cm_to_height((float)20);
			//check_height(adc_output);
			difference = adc_output - wanted_adc_output; //wanted_adc_output;
			
			if (!xQueueSend(xQueue_pwm2, &difference, 1000))
			{
				print_string("Failed to send to pwm queue potato.");
				print_string("\n\r");
			}
		}
		else
		{
			print_string("Failed to recieve from pwm queue.");
			print_string("\n\r");
		}	
	}
}

/**
		The pwm_task_rx task determines whether the ball's
		position needs to be increased or decreased.
		It then recieves the value of the difference between
		the desired ADC output and the actual ADC
		output from pwm_task_tx.
		It takes a void pointer.
		It returns nothing.
*/
static void pwm_task_rx( void *pvParameters )
{
	int difference;
	int i = 0;

	while (1)
	{
		if (xQueueReceive(xQueue_pwm2, &difference, 1000))
		{
			if (difference > 0)
			{
				pwm_adc_open();
			}
		
			else if (difference < 0)
			{
				pwm_adc_close();
			}
			
			for (i = 0; i < 100000; i++)
			{}
		}
		else
		{
			print_string("Failed to recieve from pwm queue.");
			print_string("\n\r");
		}	
	}
}

/**
		The cli_task_tx task prompts the user for input,
		displays the input, then sends the input to
		the determine_user_input function.
		It takes a void pointer.
		It returns nothing.
*/
static void cli_task_tx( void *pvParameters )
{
	//Initialize the Constant values that will be used for the Help Statement, as well as the console string.
	char CONSOLE[30] = "ENEL487_IS_AWESOME > ";

	//Initialize the loop counter, done, i and the data_array
	int i = 0;
	int j = 0;
	int length = 0;
	char data_array[50];
	int done = 0;
	
	restart(data_array);
	
	while(1)
	{	
		i = 0;
		//Initialize the data_array values to Null.
		for (i = 0; i < 50; i++)
			data_array[i] = '\0';
		
		//Print the console.
		print_string("\n\r");
		print_string(CONSOLE);
		
		//Ensure that I is set to zero.
		j = 0;
		done = 0;
					
		while (done != 1)
		{	
			//Get the user input.
			data_array[j] = getbyte();
			length++;
			
			//Check if the input was a backspace and correct for the backspace.
			if(data_array[j] == 8 || data_array[j] == 127)
			{	
				data_array[j] = '\0';
				sendbyte('\b');
				sendbyte(' ');
				sendbyte('\b');
					if (j != 0)
						j--;
					
					length--;
			}
			
			//Convert all of the values of data_array to uppercase.
			data_array[j] = TO_UPPER (data_array[j]);
			
			//Clear the value in the GPIOA_CRL register	
			* regGPIOA_CRL = 0x44444B44;
			
			sendbyte(data_array[j]);
			
			if (data_array[j] == 0xD)
			{
				sendbyte(0xD);
				done = 1;
			}
			j++;
		}

		//determine what statement was entered by the user.
		determine_user_input(data_array);
		
		//Call the restart finction to do it all again! :)
		restart(data_array);
	}
}

int fputc( int iChar, FILE *pxNotUsed ) 
{
    /* Just to avoid compiler warnings. */
    ( void ) pxNotUsed;

    if( mainDEMCR & mainTRCENA ) 
    {
        while( mainITM_Port32( 0 ) == 0 );
        mainITM_Port8( 0 ) = iChar;
    }

    return( iChar );
}
